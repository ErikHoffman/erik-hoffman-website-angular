import { Component } from '@angular/core';

@Component({
    selector: 'app-birthday-card',
    styleUrls: ['./birthday-card.css'],
    templateUrl: './birthday-card.html'
  })
  export class BirthdayCardComponent {

    nextBirthday = new Date('12/22/2019');
    remainingTime;

    ngOnInit() {
        this.findRemainingTime();
    }

    findRemainingTime() {
        setTimeout(() =>
        {
            let currentTime = new Date();
            let totalSecondsTime = (this.nextBirthday.getTime() - currentTime.getTime()) / 1000;
            let days = Math.floor(totalSecondsTime / 86400);
            totalSecondsTime = totalSecondsTime - (days * 86400);
            let hours = Math.floor(totalSecondsTime / 3600);
            totalSecondsTime = totalSecondsTime - (hours * 3600);
            let minutes = Math.floor(totalSecondsTime / 60);
            totalSecondsTime = Math.floor(totalSecondsTime - (minutes * 60));
            this.remainingTime = days + ' days, ' + hours + ' hours ' + minutes + ' minutes and ' + totalSecondsTime + ' seconds!'; 
            this.findRemainingTime();
        },
        1000);
    }
  }