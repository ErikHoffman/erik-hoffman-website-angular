import { Component, HostListener } from '@angular/core';
import { State } from './state';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
	selector: 'app-state-machine',
	styleUrls: ['./state-machine-creator.scss'],
	templateUrl: './state-machine-creator.html',
})
export class StateMachineComponent {
	createStateForm: FormGroup = new FormGroup({});
	showCreateStateWindow: boolean = false;
	showStateMenu: boolean = false;
	showDeleteStateOption: boolean = false;
	showEdgeWarning: boolean = false;
	createStateTitle: string;

	menuPos = { x: '0px', y: '0px' };
	focusedStateId: string;

	states = new Map();

	colours = [
		{ label: 'Blue', hex: '#335DFF' },
		{ label: 'Green', hex: '#10A314' },
		{ label: 'Red', hex: '#DA2A1F' },
		{ label: 'Yellow', hex: '#E3E90C' },
		{ label: 'Purple', hex: '#7613A4' },
		{ label: 'White', hex: '#FFFFFF' },
		{ label: 'Black', hex: '#000000' }
	];

	arrowSvg: string = `
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 350 100">
  			<defs>
    			<marker id="arrowhead" markerWidth="10" markerHeight="7" 
    		        refX="0" refY="3.5" orient="auto">
      				<polygon points="0 0, 10 3.5, 0 7" />
    			</marker>
			</defs>
  			<line x1="0" y1="50" x2="250" y2="50" stroke="#000" 
  				stroke-width="8" marker-end="url(#arrowhead)" />
		</svg>`;

	ngOnInit() {
		/* <html> not editble via sass */
		document.getElementsByTagName('html')[0].setAttribute('style', 'overflow: hidden');
		this.resetForm();
		this.dragElement(document.getElementById('create-state-menu'));
	}

	ngOnDestroy() {
		document.getElementsByTagName('html')[0].setAttribute('style', 'overflow: auto');
	}

	private resetForm() {
		this.createStateForm = new FormGroup({
			'name': new FormControl(''),
			'symbol': new FormControl(''),
			'accept': new FormControl(false),
			'colour': new FormControl(this.colours[0])
		});
	}

	anyMenuOpen():boolean {
		if (this.showStateMenu || this.showCreateStateWindow || this.showDeleteStateOption) {
			return true;
		}

		return false;
	}

	toggleMenu(event: any) {
		if (!this.showCreateStateWindow) {
			/* Timeout here so the host listener function below doesn't trigger AFTER this and close menu immediately */
			setTimeout(() => {
				this.showStateMenu = !this.showStateMenu;

				if (this.showStateMenu && !this.showCreateStateWindow) {
					this.focusedStateId = event.target.id;
					this.menuPos.x = event.clientX + 'px';
					this.menuPos.y = event.clientY + 'px';
				}
			});
		}
	}

	createStateOption() {
		this.showStateMenu = !this.showStateMenu;
		this.showCreateStateWindow = !this.showCreateStateWindow;
		this.createStateTitle = 'Create State';
		this.resetForm();
	}

	editStateOption() {
		this.showStateMenu = !this.showStateMenu;
		this.showCreateStateWindow = !this.showCreateStateWindow;
		this.createStateTitle = 'Edit State';
		let state: State = this.states.get(this.focusedStateId);
		this.createStateForm.patchValue({
			name: state.name,
			symbol: state.symbol,
			accept: state.accept,
			colour: state.colour
		});
	}

	createEdgeOption() {
		if (this.states.size === 1) {
			this.showEdgeWarning = true;
		} else {
			this.generateArrowSvg();
		}
	}

	deleteStateOption() {	
		this.showDeleteStateOption = true;
		this.showStateMenu = false;
	}

	createState() {
		this.showCreateStateWindow = !this.showCreateStateWindow;

		let state: State = new State();

		state.setCoordinatesFromId(this.focusedStateId);
		state.name = this.createStateForm.value.name;
		state.symbol = this.createStateForm.value.symbol;
		state.accept = this.createStateForm.value.accept;
		state.colour = this.createStateForm.value.colour;

		this.states.set(this.focusedStateId, state);
		this.resetForm();
		this.focusedStateId = null;
	}

	deleteState() {
		this.showDeleteStateOption = false;
		let indexOfRemove: number = null;

		this.states.forEach((state, index) => {
			if (state.getCoordinatesAsId() === this.focusedStateId) {
				indexOfRemove = index;
			}
		});

		if (indexOfRemove !== null) {
			document.getElementById(this.focusedStateId).style.backgroundColor = null;
			this.states.delete(this.focusedStateId);
		}

		this.focusedStateId = null;
	}

	stateDoesNotExist(): boolean {
		let doesNotExist = true;
		this.states.forEach((s) => {
			if (s.getCoordinatesAsId() === this.focusedStateId) {
				doesNotExist = false;
			}
		});
		return doesNotExist;
	}

	public hideAllWindows() {
		this.showCreateStateWindow = false;
		this.showDeleteStateOption = false;
		this.showEdgeWarning = false;
		this.showStateMenu = false;
		this.focusedStateId = null;
	}

	generateArrowSvg() {
		var arrowDiv = document.createElement('div');
		arrowDiv.style.position = 'relative';
		let focusedState = document.getElementById(this.focusedStateId);
		let focusedStateAsCoords = this.getCoordinatesFromId(this.focusedStateId);
		let stateArea = document.getElementById('state-area');
		let top = -stateArea.scrollHeight + focusedStateAsCoords.x * 47;
		let left = focusedStateAsCoords.y * 47;
		
		arrowDiv.style.top = top.toString();
		document.getElementById('state-area').appendChild(arrowDiv);
		let arrowSvg = `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 350 100" 
			style="position:absolute;top:${top};left:${left};pointer-events:none">
			<defs>
		  		<marker id="arrowhead" markerWidth="10" markerHeight="7" 
		  				refX="0" refY="3.5" orient="auto">
					<polygon points="0 0, 10 3.5, 0 7" />
		  		</marker>
			</defs>
			<line x1="0" y1="50" x2="250" y2="50" stroke="#000" 
				  stroke-width="8" marker-end="url(#arrowhead)" />
		  </svg>`;
		  arrowDiv.innerHTML = arrowSvg;

		// document.createElementNS(`http://www.w3.org/2000/svg`, arrowSvg);
	}

	getCoordinatesFromId(id: string): { x: number, y: number } {
		let x = parseInt(id.split(',')[0]);
		let y = parseInt(id.split(',')[1]);
		return { x: x, y: y };
	}

	@HostListener('click', ['!$event.target'])
	clickOutsideStateMenu() {
		let stateMenu = document.getElementById('state-menu');
		var isClickInside = stateMenu.contains(event.target as Node);
	
		if (!isClickInside && this.showStateMenu) {
			this.showStateMenu = false;
			this.focusedStateId = null;
		}
	}

	/* UI helper functions */
	dragElement(elmnt) {
		var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
		if (document.getElementById('create-state-title')) {
			/* if present, the header is where you move the DIV from:*/
			document.getElementById('create-state-title').onmousedown = dragMouseDown;
		} else {
			/* otherwise, move the DIV from anywhere inside the DIV:*/
			elmnt.onmousedown = dragMouseDown;
		}

		function dragMouseDown(e) {
			e = e || window.event;
			e.preventDefault();
			// get the mouse cursor position at startup:
			pos3 = e.clientX;
			pos4 = e.clientY;
			document.onmouseup = closeDragElement;
			// call a function whenever the cursor moves:
			document.onmousemove = elementDrag;
		}

		function elementDrag(e) {
			e = e || window.event;
			e.preventDefault();
			// calculate the new cursor position:
			pos1 = pos3 - e.clientX;
			pos2 = pos4 - e.clientY;
			pos3 = e.clientX;
			pos4 = e.clientY;
			// set the element's new position:
			elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
			elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
		}

		function closeDragElement() {
			/* stop moving when mouse button is released:*/
			document.onmouseup = null;
			document.onmousemove = null;
		}
	}
}


