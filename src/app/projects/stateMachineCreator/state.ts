export class State {
	coordinates: { x: number, y:number } = { x: null, y: null };
	name: string;
	symbol: string;
	accept: boolean;
	colour: string;
	edges: Edge[];

	getCoordinatesAsId(): string {
		return `${this.coordinates.x},${this.coordinates.y}`;
	};

	setCoordinatesFromId(id: string) {
		this.coordinates.x = parseInt(id.split(',')[0]);
		this.coordinates.y = parseInt(id.split(',')[1]);
	}
};

export class Edge {
	source: { x: number, y: number };
	destination: { x: number, y: number };
	name: string;
};