export class Card {
	suit: string;
	numberSymbol: string;
	constructor(suitName: string, numberSymbolName: string) {
		this.suit = suitName;
		this.numberSymbol = numberSymbolName;
	}
}

export class Pile {
	cards: Card[];
	public shuffle() {
		var count = this.cards.length,
				randomnumber,
				temp;
		while (count){
			randomnumber = Math.random() * count-- | 0;
			temp = this.cards[count];
			this.cards[count] = this.cards[randomnumber];
			this.cards[randomnumber] = temp;
		}
	}

	/* Checks incoming card against the numbers already placed in this suit */
	public numberMatch(card: Card) {
		for (let i = 0; i < this.cards.length; i++) {
			if (card.numberSymbol === this.cards[i].numberSymbol) {
				return true;
			}
		}
		return false;
	}
	
	constructor() {
		this.cards = [];
	}
}
