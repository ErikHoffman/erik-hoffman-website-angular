import { async } from '@angular/core/testing';
import { OsmosisSolitaireComponent } from './osmosis-solitaire-component';
import { Card, Pile } from './card-class';

describe('OsmosisSolitaireComponent', () => {
	it('should place card that matches first suit to that suit', async(() => {
		const comp = new OsmosisSolitaireComponent();
		let piles: Pile[] = new Array<Pile>();
		for (let i = 0; i < 4; i++) {
			piles.push(new Pile());
		}

		piles[0].cards.push(new Card('A', 'S'));

		let placeFromPile: Pile = new Pile();
		placeFromPile.cards.push(new Card('2', 'S'));

		let numPlaced: number = comp.placeFromPile(placeFromPile, piles);

		expect(numPlaced).toBe(1);
	}));

	it('should place multiple cards that match first suit to that suit', async(() => {
		const comp = new OsmosisSolitaireComponent();
		let piles: Pile[] = new Array<Pile>();
		for (let i = 0; i < 4; i++) {
			piles.push(new Pile());
		}
		
		piles[0].cards.push(new Card('S', 'A'));

		let placeFromPile: Pile = new Pile();
		placeFromPile.cards.push(new Card('S', '2'));
		placeFromPile.cards.push(new Card('S', '3'));
		placeFromPile.cards.push(new Card('S', '4'));

		let numPlaced: number = comp.placeFromPile(placeFromPile, piles);

		expect(numPlaced).toBe(3);
		expect(placeFromPile.cards.length).toBe(0);
	}));

	it('should place card that matches its number in first 3 suits into 4th', async(() => {
		const comp = new OsmosisSolitaireComponent();
		let piles: Pile[] = new Array<Pile>();
		for (let i = 0; i < 4; i++) {
			piles.push(new Pile());
		}
		
		piles[0].cards.push(new Card('S', 'A'));
		piles[1].cards.push(new Card('C', 'A'));
		piles[2].cards.push(new Card('H', 'A'));

		let placeFromPile: Pile = new Pile();
		placeFromPile.cards.push(new Card('D', 'A'));

		let numPlaced: number = comp.placeFromPile(placeFromPile, piles);

		expect(numPlaced).toBe(1);
	}));


	it('should place cards from the input pile sequentially if the cards placed before them open them up for placement', () => {
		const comp = new OsmosisSolitaireComponent();
		let piles: Pile[] = new Array<Pile>();

		for (let i = 0; i < 4; i++) {
			piles.push(new Pile());
		}

		let placeFromPile: Pile = new Pile();
		placeFromPile.cards.push(new Card('C', '2'));
		placeFromPile.cards.push(new Card('S', '2'));

		piles[0].cards.push(new Card('S', 'A'));

		let numPlaced: number = comp.placeFromPile(placeFromPile, piles);

		expect(placeFromPile.cards.length).toBe(0);
		expect(numPlaced).toBe(2);
	});


	it('should not place card to second suit that does not match any number in first', () => {
		const comp = new OsmosisSolitaireComponent();
		let piles: Pile[] = new Array<Pile>();
		for (let i = 0; i < 4; i++) {
			piles.push(new Pile());
		}

		piles[0].cards.push(new Card('A', 'S'));

		let placeFromPile: Pile = new Pile();
		placeFromPile.cards.push(new Card('2', 'C'));

		let numPlaced: number = comp.placeFromPile(placeFromPile, piles);

		expect(numPlaced).toBe(0);
	});
})
