import { Component } from '@angular/core';
import { Card, Pile } from './card-class';

@Component({
	selector: 'app-osmosis-solitaire',
	styleUrls: ['./osmosis-solitaire.scss'],
	templateUrl: './osmosis-solitaire.html'
})
export class OsmosisSolitaireComponent {

	constructor() { }

	/* Dropdown, default to 1000 games */
	gamesOptions = [
		{ id: 100, name: "100 Games" },
		{ id: 1000, name: "1000 Games" },
		{ id: 10000, name: "10000 Games" },
		{ id: 100000, name: "100000 Games" },
		{ id: 1000000, name: "1000000 Games" }
	];

	selectedValue = this.gamesOptions[1];
	/*************/

	suitsChars: string[] = ['S', 'C', 'H', 'D'];
	numbers: string[] = ['A', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K'];

	/* Statistics */
	wins: number = 0;
	gamesRan: number = 0;
	averageLosingSuitCountAccumulator: number = 0;
	averageLoopsThroughDeckAccumulator: number = 0;
	averageLosingSuitCount: number;
	averageLoopsThroughDeck: number;
	winPercentage: number;
	runPercent: number;
	lastWinningSuits: Pile[];
	/**************/

	elementNames: string[] = [
		'suitOne',
		'suitTwo',
		'suitThree',
		'suitFour'
	];

	gamesRunning = false;

	toggleRunning(toggle: boolean) {
		return new Promise(resolve => {
			setTimeout(() => {
				this.gamesRunning = toggle;
				resolve(true);
			});
		});
	}

	countSuits = (accumulator: any, currentVal: any) => {
		if (accumulator instanceof Pile) { accumulator = accumulator.cards.length; } // Initialize, don't want to reduce whole object
		return accumulator + currentVal.cards.length;
	}

	/* Main entry point when "Run" button is clicked */
	async runGames() {
		this.runPercent = 0;
		this.toggleRunning(true)
		.then(async () => {
			for (let x = 0; x < this.selectedValue.id; x++) {
				await this.gameLoop(x);
			}

			/* Once games have run, turn loading off */
			this.toggleRunning(false);
			this.handleWin(this.lastWinningSuits);

			/* Update statistic on the simulation(s):
				total games ran, highest num of cards placed into suits and win % */
			this.gamesRan += this.selectedValue.id;
			this.averageLosingSuitCount = Math.floor(this.averageLosingSuitCountAccumulator / this.gamesRan);
			this.averageLoopsThroughDeck = Math.floor(this.averageLoopsThroughDeckAccumulator / this.gamesRan);
			this.winPercentage = this.wins / this.gamesRan * 100;
		});
	}

	async gameLoop(gameNum: number) {
		return new Promise(resolve => {
			let deck = new Pile();
			let piles = [];
			let suits = [];

			for (let i = 0; i < 4; i++) {
				suits[i] = new Pile();
			}

			let stackFlipped = new Pile();

			/* Setup the game */
			/* Create the deck */
			for (let i = 0; i < this.suitsChars.length; i++) {
				for (let j = 0; j < this.numbers.length; j++) {
					let card: Card;
					card = new Card(this.suitsChars[i], this.numbers[j]);
					deck.cards.push(card);
				}
			}

			deck.shuffle();
			// let deckCopy = deck.cards.map(function (x) { return x; }); // Use at end to show untouched winning deck

			/* Create the 4 piles */
			for (let i = 0; i < 4; i++) {
				piles[i] = new Pile();
				for (let j = 0; j < 4; j++) {
					piles[i].cards.push(deck.cards.pop());
				}
			}

			/* Take the very top card of the deck and use that as our base for the first suit */
			suits[0].cards.push(deck.cards.pop());

			/* Game is now setup and commences */

			/* Flip 3 cards into the flip pile to start */
			for (let i = 0; i < 3; i++) {
				stackFlipped.cards.push(deck.cards.pop());
			}

			let numPlacedThisDeckLoop: number = 0;
			let loopsThroughDeck: number = 0;

			while (true) {
				let numPlaced: number = 0;

				for (let i = 0; i < 4; i++) {
					/* Loop through the 4 non-deck piles. If they are not already empty, try to place */
					if (piles[i].cards.length > 0) {
						let numPlacedFromPile: number = this.placeFromPile(piles[i], suits);
						if (numPlacedFromPile > 0) {
							numPlacedThisDeckLoop += numPlacedFromPile;
							numPlaced += numPlacedFromPile;
						}
					}
				}

				/* If we placed anything, loop back to check the piles again before moving on to the flip pile */
				if (numPlaced > 0) {
					continue;
				}

				/* If there are any cards in the flip pile, try to place from the top first */
				if (stackFlipped.cards.length > 0) {
					let numPlacedFromPile: number = this.placeFromPile(stackFlipped, suits);
					if (numPlacedFromPile > 0) {
						numPlacedThisDeckLoop += numPlacedFromPile;
						continue;
					}
				}
				
				if (deck.cards.length > 0) {
					/* Flip 3 (or 2 or 1 if only that many cards remain in the deck) cards into the flip pile */
					for (let i = 0; i < (deck.cards.length >= 3 ? 3 : deck.cards.length); i++) {
						stackFlipped.cards.push(deck.cards.pop());
					}

					/* Give those last cards a chance at being placed before ending the game */
					if (deck.cards.length === 0) {
						continue;
					}
				}

				if (suits.reduce(this.countSuits) === 52) {
					/* Suits are full, this is a win */
					break;
				}

				if (deck.cards.length === 0) { /* The deck is empty, take the flipped pile and turn it back into the deck */
					if (numPlacedThisDeckLoop == 0) {
						break;
					}

					loopsThroughDeck++;
					numPlacedThisDeckLoop = 0; /* Reset this number, if we make it through the entire next deck without placing a card from it or the 4 piles,
						the game is a loss */

					while(stackFlipped.cards.length > 0) {
						deck.cards.push(stackFlipped.cards.pop());
					}

					/* Flip 1-3 cards onto the flip pile to start it off again */
					for (let i = 0; i < (deck.cards.length >= 3 ? 3 : deck.cards.length); i++) {
						stackFlipped.cards.push(deck.cards.pop());
					}
				}
			}

			if (suits.reduce(this.countSuits) === 52) {
				this.lastWinningSuits = suits;
				this.wins++;
			} else {
				this.averageLosingSuitCountAccumulator += suits.reduce(this.countSuits);
				this.averageLoopsThroughDeckAccumulator += loopsThroughDeck;
			}

			if (gameNum % (this.selectedValue.id / 100) === 0) {
				setTimeout(() => {
					this.runPercent = Math.floor((gameNum / this.selectedValue.id) * 100);
					resolve(true);
				});
			} else {
				resolve(true);
			}
		});
	}

	placeFromPile(pile: Pile, suits: Pile[]) {
		let numPlaced: number = 0;
		while (pile.cards.length > 0) {
			let cardNumExistsInPrevSuit: boolean = true;
			let cardSuit: string = pile.cards[pile.cards.length - 1].suit;

			// Now we loop through the 4 suits, trying to place the card
			for (let suitIndex = 0; suitIndex < 4; suitIndex++) {
				if (suitIndex === 0 && cardSuit === suits[0].cards[0].suit) { // Handle first suit, which has no rules except the card must match the suit
					suits[0].cards.push(pile.cards.pop());
					numPlaced++;
					break;
				} else if (suitIndex > 0 && suits[suitIndex].cards.length === 0 && suits[suitIndex - 1].cards.length > 0 &&
							cardNumExistsInPrevSuit) { // Empty suit check for 2, 3, 4, only check if previous suit is not empty
					suits[suitIndex].cards.push(pile.cards.pop());
					numPlaced++;
					break;
				} else if (suitIndex > 0 && suits[suitIndex].cards.length > 0 && 
							cardNumExistsInPrevSuit && (cardSuit === suits[suitIndex].cards[0].suit)) { // If suit is not empty
					suits[suitIndex].cards.push(pile.cards.pop());
					numPlaced++;
					break;
				}

				/* This checks if the card's number is found in the current suit, even though the suit doesn't match.
					If it is, that means it can then be placed in a subsequent suit */
				if (suitIndex < 3 && suits[suitIndex].cards.length > 0 && pile.cards.length > 0) {
					cardNumExistsInPrevSuit = suits[suitIndex].numberMatch(pile.cards[pile.cards.length - 1]);
				} else {
					/* No point in continuing, card is not placeable, return */
					return numPlaced;
				}
			}
		}
		return numPlaced // Out of cards in the pile so return status
	}

	/* Run DOM manipulation after a win is found */
	handleWin(suits: Pile[]) {
		this.resetWinningSuitsDivs();

		for (let s = 0; s < 4; s++) {
			if (suits[s].cards.length > 0) {
				this.appendCardElement(s, this.elementNames[s], true, suits);
			}
		}

		this.wins++;
	}

	/* resetWinningSuitsDivs - empties the divs where the cards are placed */
	resetWinningSuitsDivs() {
		for (let i = 0; i < 4; i++) {
			var suitNode = document.getElementById(this.elementNames[i]);
			while (suitNode.firstChild) {
				suitNode.removeChild(suitNode.firstChild);
			}
		}
	}

	/* appendCardElement - Show the cards that the player won with
	 * @suitNum - which suit pile to check
	 * @suitStr - the suit that the pile is
	 * @endGame - if it's the end of the game and need to display */
	appendCardElement(suitNum: number, suitStr: string, endGame: boolean, suits: Pile[]) {
		let suitChar = suits[suitNum].cards[0].suit;
		if (endGame) {
			for (let i of suits[suitNum].cards) {
				let suitElem = document.createElement('img');
				suitElem.setAttribute('style', 'position:relative;width:7.5%;height:auto');
				suitElem.setAttribute('src', '../assets/Vector-Playing-Cards-master/cards-svg/'
					+ i.numberSymbol + suitChar + '.svg');
				document.getElementById(suitStr).appendChild(suitElem);
			}
		}
	}
}
