import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title: string = 'Erik Hoffman Portfolio Site';

  constructor(
    private titleService: Title,
    private metaService: Meta
  ) {}

  ngOnInit() {
    this.titleService.setTitle(this.title);
    this.metaService.addTags([
      {name: 'keywords', content: 'Angular, Universal, Portfolio, ErikHoffman'},
      {name: 'description', content: 'Angular Universal ErikHoffman Portfolio'},
      {name: 'robots', content: 'index, follow'}
    ]);
  }


  // constructor(
  //   private route: ActivatedRoute
  // ) {
  // }
}
