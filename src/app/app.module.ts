import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { PageContentComponent } from './page-content/page-content.component';
import { PortfolioContentComponent } from './directives/portfolio-content/portfolio-content.component';
import { ResumeContentComponent } from './directives/resume-content/resume-content.component';
import { OsmosisSolitaireComponent } from './projects/osmosisSolitaire/osmosis-solitaire-component';
import { StateMachineComponent } from './projects/stateMachineCreator/state-machine-creator-component';
import { BirthdayCardComponent } from './projects/birthdayCard/birthday-card-component';

import { AppRoutingModule } from './app-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

/* Material */
import { MatCheckboxModule } from '@angular/material/checkbox';

@NgModule({
  declarations: [
    AppComponent,
    PageContentComponent,
    PortfolioContentComponent,
    ResumeContentComponent,
    OsmosisSolitaireComponent,
    StateMachineComponent,
    BirthdayCardComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
	  AppRoutingModule,
	  ReactiveFormsModule,
    NoopAnimationsModule,
    MatCheckboxModule
  ],
  providers: [
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
