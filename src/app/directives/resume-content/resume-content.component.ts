import { Component, Input } from '@angular/core';
import { ResumeItem } from './resumeItems';

@Component({
	selector: 'app-resume-section',
	templateUrl: './resume-content.component.html',
	styleUrls: ['./resume-content.component.scss']
})
export class ResumeContentComponent {
	@Input() resumeItem: ResumeItem;
}
