export const resumeItems: ResumeItem[] = [
	{
		name: 'Qualifications',
		bulletColour: 'blue',
		sections: [{
			name: null,
			startDate: null,
			endDate: null,
			bullets: [
				'Multiple years of programming in C, Python, Java, HTML, CSS, Javascript/EcmaScript, Typescript',
				'Strong skills in frontend frameworks; Angular, AngularJS and React as well as some jsp for legacy systems',
				'Strong skills in ExpressJS and Java backends as well as microservice paradigms',
				'Intermediate skills in Amazon Web Services and Microsoft Azure',
				'Focus on test-driven development (JUnit, Jasmine, Karma) to ensure business logic is not broken by new code',
				'Linux/Windows command line, bash scripts to automate tasks and improve work speed',
				'Git and SVN experience in a professional, multi-project setting',
				'Knowledge of Jenkins, Jira, Bitbucket, Confluence, Trello',
				'Writing of technical documents, deployment instructions and tutorials whenever I feel they are helpful'
			]}
		]
	},
	{
		name: 'Work Experience',
		bulletColour: 'green',
		sections: [
			{
				name: 'Smart Solution - Web Developer (Full Stack)',
				startDate: 'April 2019',
				endDate: 'Present',
				bullets: [
					'Wrote full stack solutions for an industry leading banking platform and member-facing applications',
					'Used new tech such as Ionic and Angular 2+/VueJS and maintained legacy Java projects in Spring/Springboot',
					'Gave talks to entire team on upgrading tech stacks and paradigms in order to improve workflow',
					'Performed BA role by writing every use case for a modern mobile banking app replacement',
					'Worked with business and design team to develop the most client-friendly solutions for features and bug fixes'
				]
			},
			{
				name: 'CIBC - Intermediate Applications Developer',
				startDate: 'February 2017',
				endDate: 'April 2019',
				bullets: [
					'Wrote full stack solutions for a client-facing application in AngularJS, ExpressJS, Jasmine, Java Spring',
					'Part of agile development team, using Jira, Bitbucket and Confluence for workflow',
					'Wrote deployment scripts in Jenkins and wrote technical documents on how to maintain environments'
				]
			},
			{
				name: 'University of Guelph - Teaching Assistant for Introduction to Computing',
				startDate: 'September 2016',
				endDate: 'December 2016',
				bullets: [
					'Assist students with learning basics of applications: Word, Excel, Powerpoint and Access, Javascript',
					'Conducted office hours, answered student questions at any time and graded assignments'
				]
			}
		]
	},
	{
		name: 'Education',
		bulletColour: 'red',
		sections: [{
			name: 'University of Guelph',
			startDate: 'Fall 2012',
			endDate: 'Fall 2016',
			bullets: [
				'Computer Science Honours',
				'Mathematics Minor',
				'Notable courses completed: Data Structures, Networking and Web Development, Operating Systems, Discrete Mathematics, Combinatorics and Graph Theory, Algorithms, Security (RSA and prime mathematics), Microprocessors and Assembly Language, Theory of Computation, Databases, Artificial Intelligence, File Storage Structures and Data Retrieval.'
			]
		}]
	}
];

export class ResumeItem {
	name: string;
	bulletColour: string;
	sections: Array<{
		name: string;
		startDate: string;
		endDate: string;
		bullets: Array<string>;
	}>
}
