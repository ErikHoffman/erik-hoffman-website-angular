import { Component } from '@angular/core';

@Component({
	selector: 'app-portfolio-section',
	inputs: ['name', 'description', 'dates', 'localLink', 'link'],
	templateUrl: './portfolio-content.component.html',
	styleUrls: ['./portfolio-content.component.scss']
})
export class PortfolioContentComponent {
	name: string;
	description: string;
	localLink: string;
	link: string;
	dates: string;
}
