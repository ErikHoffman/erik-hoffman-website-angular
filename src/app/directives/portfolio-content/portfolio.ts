export const projects:Project[] = [
  { id: 1,
    name: 'State Machine Creator',
    description: 'A UI that lets you create simple state machines and save them to a file.' +
    '<br><br>I enjoy using state machines to create very basic workflows in order to plan applications or algorithms' +
    ' so making this tool seemed to make sense.',
    dates: '2015 - Ongoing',
    localLink: '../projects/state-machine-creator',
    link: ''
  },
  { id: 2,
    name: 'Osmosis Solitaire Simulator',
    description: 'Osmosis or Treasure Trove is a very easy to play but also hard to win Solitaire variation that is essentially a test' +
    ' of observation to see if you can not miss any potential moves.' +
    '<br><br>According to sites that gather data on the game and some test games played myself, the human win ratio sits around 10% while the simulator' +
    ' finds that the ratio is actually around 17% if every move is noticed.',
    dates: '2016 - 2018',
    localLink: '../projects/osmosis-solitaire',
    link: ''
  },
  { id: 3,
    name: 'Functional Connect Four in Elixir',
    description: 'Created in a functional programming study group during my final semester at University of Guelph.' +
    '<br><br>Used the Elixir programming language to create a connect four game to communicate over IRC with other members of the class.' +
    ' Learned about the advantages of functional as well as the hurdles one must overcome (mutability, recursion, etc.).',
    dates: '2016',
    localLink: '',
    link: 'https://bitbucket.org/ErikHoffman/connect-four-elixir'
  },
  { id: 4,
    name: 'A* Search in Python',
    description: 'Finding optimal paths for movement in a 2D space in Python.' +
    '<br><br>Done while studying movement algorithms and implementation for RTS games.',
    dates: '2016',
    localLink: '',
    link: 'https://bitbucket.org/ErikHoffman/a-search-rts/src/master/'
  },
  { id: 5,
    name: 'Truform - Android Workout App',
    description: 'Group project for a course at University of Guelph that followed the development of' +
    ' an Android app which was developed using agile methods.' +
    '<br><br>Created using an Arduino computer with a gyroscope and accelerometer' +
    ' linked to a smartphone to measure workout form.' +
    ' My role in the project was creating the algorithm that measures the data of' +
    ' the workout and gives output on whether or not the form of the' +
    ' workout was performed well.',
    dates: '2015',
    localLink: '',
    link: 'https://bitbucket.org/benbaird/truform'
  }
];

export class Project {
  id: number;
  name: string;
  description: string;
  dates: string;
  localLink: string;
  link: string;
}
