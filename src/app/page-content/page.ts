export const pages:Page[] = [
  { id: 1, name: 'About Me', route: 'Home', class: 'header-icon about-icon' },
  { id: 2, name: 'Projects', route: 'Projects', class: 'header-icon projects-icon' },
  { id: 3, name: 'Resume', route: 'Resume', class: 'header-icon resume-icon' }
];

export class Page {
  id: number;
  name: string;
  route: string;
  class: string;
}
