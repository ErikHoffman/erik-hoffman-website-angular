/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { RouterTestingModule } from '@angular/router/testing';

import { PageContentComponent } from './page-content.component';

describe('PageContentComponent', () => {
	let component: PageContentComponent;
	let fixture: ComponentFixture<PageContentComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [RouterTestingModule],
			declarations: [
				PageContentComponent
			],
		}).compileComponents();
	}));


	beforeEach(() => {
		fixture = TestBed.createComponent(PageContentComponent);
		component = fixture.debugElement.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
