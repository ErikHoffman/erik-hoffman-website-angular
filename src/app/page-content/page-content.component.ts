import { Component, OnInit } from '@angular/core';
import { Location, PlatformLocation } from '@angular/common';
import { Page, pages } from './page';
import { projects } from '../directives/portfolio-content/portfolio';
import { resumeItems } from '../directives/resume-content/resumeItems';
import { Router, NavigationStart, ActivatedRoute } from '@angular/router';
import { NgZone } from '@angular/core';
import { Subscription, fromEvent } from 'rxjs';

@Component({
	selector: 'app-page-content',
	templateUrl: './page-content.component.html',
	styleUrls: ['./page-content.component.scss']
})
export class PageContentComponent implements OnInit {

	selectedPage: Page = {
		id: -1,
		name: 'unrouted',
		route: 'unrouted',
		class: ''
	};
	defaultPage = pages[0];
	pagesList = pages;
	projectsList = projects;
	resumeItems = resumeItems;
	showDropdown = false;
	routedPage: string;

	subscriptions = new Subscription;

	constructor(
		public zone: NgZone,
		public router: Router,
		private location: Location,
		private platformLocation: PlatformLocation) {
			this.subscriptions.add(this.router.events.subscribe((e: any) => {
				if (e instanceof NavigationStart) {
					this.routeToPage(e);
				}
			}));
	
			/* Subscribe to backbutton since it does not trigger router event */
			this.subscriptions.add(this.platformLocation.onPopState(() => {
				this.routeToPage();
			}));

			this.routeToPage();
		}

	ngOnInit() {
	}

	routeToPage(event?: any) {
		this.routedPage = event ? event.url.slice(1) : window.location.hash.slice(2);

		Object.keys(this.pagesList).forEach(key => {
			if (this.pagesList[key].route.toUpperCase() === this.routedPage.toUpperCase()) {
				this.selectedPage = this.pagesList[key];
			}
		});

		if (this.selectedPage.id === -1) {
			this.selectedPage = this.defaultPage;
		}
	}

	hamburgerClick() {
		this.showDropdown = !this.showDropdown;
		document.getElementById('hamburger').classList.toggle('change');
	}

	onSelect(page: Page, dropdown: Boolean): void {
		this.selectedPage = page;
		this.showDropdown = false;
		if (dropdown) {
			document.getElementById('hamburger').classList.toggle('change');
			document.getElementById('mobile-dropdown-main').style.display = 'none';
		}
		// this.router.navigateByUrl(`/${this.selectedPage.route.toLowerCase}`)
		this.location.go(`/${this.selectedPage.route.toLowerCase()}`);
	}

	ngOnDestroy() {
		this.subscriptions.unsubscribe();
	}

}
