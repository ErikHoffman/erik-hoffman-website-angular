import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { pages } from './page-content/page';

import { PageContentComponent } from './page-content/page-content.component';
import { OsmosisSolitaireComponent } from './projects/osmosisSolitaire/osmosis-solitaire-component';
import { StateMachineComponent } from './projects/stateMachineCreator/state-machine-creator-component';
import { BirthdayCardComponent } from './projects/birthdayCard/birthday-card-component';

const routes: Routes  = [
  { path: '', redirectTo: '/home', pathMatch: 'full'},
  { path: 'home', component: PageContentComponent, data: { page: pages[0] }},
  { path: 'projects', component: PageContentComponent, data: { page: pages[1] }, children: [
    { path: 'osmosis-solitaire', component: OsmosisSolitaireComponent },
    { path: 'state-machine-creator', component: StateMachineComponent },
    { path: 'happybirthday', component: BirthdayCardComponent }
  ]},
  { path: 'resume', component: PageContentComponent, data: { page: pages[2] }},
];

@NgModule({
  imports: [ RouterModule.forRoot(routes, {useHash: true}) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
